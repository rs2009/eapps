function get_app_info () {
    $.getJSON('https://api.cleanapk.org/v2/apps?action=app_detail&id=' + window.location.hash.slice(1), function (data) {
        // Set app title.
        $('#app-title').text(data.app.name);

        // Load app icon.
        $('#app-icon').attr('src', 'https://api.cleanapk.org/v2/media/' + data.app.icon_image_path)

        // Load app summary
        $('#app-summary').text(data.app.description)
    })
}

function install () {
    window.location.href = 'http://' + window.location.hostname + ':2230/?appid=' + window.location.hash.slice(1)
}

get_app_info()