function get_top_apps () {
    $.getJSON('https://api.cleanapk.org/v2/apps?action=list_home&type=pwa', function (data) {
        window.home = data.home

        // Apps
        let app_title_i = 0
        $('[id^="app-card"][id$="-title"]').each(function () {
            $('#' + this.id).text(data.home.popular_apps[app_title_i].name)
            app_title_i++
        })

        let app_img_i = 0
        $('[id^="app-card"][id$="-img"]').each(function () {
            $('#' + this.id).attr('src', 'https://api.cleanapk.org/v2/media/' + data.home.popular_apps[app_img_i].icon_image_path)
            app_img_i++
        })

        let app_summary_i = 0
        $('[id^="app-card"][id$="-summary"]').each(function () {
            $('#' + this.id).text(data.home.popular_apps[app_summary_i].description)
            app_summary_i++
        })
        
        // Games
        let game_title_i = 0
        $('[id^="game-card"][id$="-title"]').each(function () {
            $('#' + this.id).text(data.home.popular_games[game_title_i].name)
            game_title_i++
        })

        let game_img_i = 0
        $('[id^="game-card"][id$="-img"]').each(function () {
            $('#' + this.id).attr('src', 'https://api.cleanapk.org/v2/media/' + data.home.popular_games[game_img_i].icon_image_path)
            game_img_i++
        })

        let game_summary_i = 0
        $('[id^="game-card"][id$="-summary"]').each(function () {
            $('#' + this.id).text(data.home.popular_games[game_summary_i].description)
            game_summary_i++
        })
    })
}

function get_apps () {
    $('#container').append(`
        <div class="row">
            <h1>Popular apps in the last 24 hours</h1>
        </div>
        <div class="row mt-4"></div>
    `)

    // 'i' and 'ii' are counters and this is not minified JS.
    let i = 1;
    let app = 1;
    
    while (i <= 2) {
        let ii = 1;

        $('#container').append(`<div class='row mt-4' id="app_row` + i + `">`)

        while (ii <= 6) {
            $('#app_row' + i).append(`
                <div class="col">
                    <div class="card" style="width: 10rem; padding: 16px; margin-bottom: 16px;">
                    <img id="app-card` + app + `-img" style="margin: 40px;">
                    <div class="card` + app + `-body">
                        <h5 class="card` + app + `-title" id="app-card` + app + `-title">Loading..</h5>
                        <p class="card` + app + `-text" id="app-card` + app + `-summary">Loading...</p>
                        <button onclick="check_out('` + app + `', 'app')" class="btn btn-primary install">Checkout</button>
                    </div>
                </div>
            `)

            app++
            ii++
        }

        $('#container').append(`</div>`)

        i++
    }

    $('#container').append('<div class="row mt-4">')
}

function get_games () {
    $('#container').append(`
        <div class="row">
            <h1>Popular games in the last 24 hours</h1>
        </div>
        <div class="row mt-4"></div>
    `)

    // 'i' and 'ii' are counters and this is not minified JS.
    let i = 1;
    let game = 1;
    
    while (i <= 2) {
        let ii = 1;

        $('#container').append(`<div class='row' id="game_row` + i + `">`)

        while (ii <= 6) {
            $('#game_row' + i).append(`
                <div class="col">
                    <div class="card" style="width: 10rem; padding: 16px; margin-bottom: 16px;">
                    <img id="game-card` + game + `-img" style="margin: 40px;">
                    <div class="card` + game + `-body">
                        <h5 class="card` + game + `-title" id="game-card` + game + `-title">Loading..</h5>
                        <p class="card` + game + `-text" id="game-card` + game + `-summary">Loading...</p>
                        <button onclick="check_out('` + game + `', 'game')" class="btn btn-primary install">Checkout</button>
                    </div>
                </div>
            `)

            game++
            ii++
        }

        $('#container').append(`</div>`)

        i++
    }

    $('#container').append(`<div class='row mt-4</div>`)
}

function check_out (i, app_type) {
    if (app_type == 'game')
        window.location = 'pages/app-view.html#' + window.home.popular_games[i-1]._id
    else
        window.location = 'pages/app-view.html#' + window.home.popular_apps[i-1]._id
}

$(document).ready(function() {
    get_apps()
    get_games()
    get_top_apps()
})