function search () {
    $('#card1').css('display', 'none')
    $('#card2').css('display', 'none')
    $('#card3').css('display', 'none')
    $('#card4').css('display', 'none')

    $.getJSON('https://api.cleanapk.org/v2/apps?action=search&keyword=' + encodeURI($('#search-text').val()) + '&nres=4&type=pwa', function (data) {
        window.apps = data.apps

        // App 1
        $('#card1-title').text(data.apps[0].name);
        $('#card1-img').attr('src', 'https://api.cleanapk.org/v2/media/' + data.apps[0].icon_image_path)
        $('#card1-summary').text(data.apps[0].description)

        if (data.apps[0] != undefined)
            $('#card1').css('display', 'flex')

        // App 2
        $('#card2-title').text(data.apps[1].name);
        $('#card2-img').attr('src', 'https://api.cleanapk.org/v2/media/' + data.apps[1].icon_image_path)
        $('#card2-summary').text(data.apps[1].description)

        if (data.apps[1] != undefined)
            $('#card2').css('display', 'flex')

        // App 3
        $('#card3-title').text(data.apps[2].name);
        $('#card3-img').attr('src', 'https://api.cleanapk.org/v2/media/' + data.apps[2].icon_image_path)
        $('#card3-summary').text(data.apps[2].description)

        if (data.apps[2] != undefined)
            $('#card3').css('display', 'flex')

        // App 4
        $('#card4-title').text(data.apps[3].name);
        $('#card4-img').attr('src', 'https://api.cleanapk.org/v2/media/' + data.apps[3].icon_image_path)
        $('#card4-summary').text(data.apps[3].description)
        
        if (data.apps[3] != undefined)
            $('#card4').css('display', 'flex')
    })
}

function check_out (i) {
    window.location = '../pages/app-view.html#' + window.apps[i]._id
}