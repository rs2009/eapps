# Dockerfile for /e/ Apps

FROM nginx:latest

MAINTAINER rs2009@ubuntu.com

RUN apt-get update -y && apt-get install -y wget python3 python3-flask bash --no-install-recommends

WORKDIR /bin

RUN wget https://raw.githubusercontent.com/Ubuntu-Web/wadk/master/create-app && chmod 755 create-app
RUN wget https://raw.githubusercontent.com/Ubuntu-Web/wadk/master/build-app && chmod 755 build-app
RUN mkdir /wapp-gen

COPY . /usr/share/nginx/html

COPY wapp-gen /wapp-gen
COPY docker-run /usr/bin

ENTRYPOINT bash /usr/bin/docker-run
